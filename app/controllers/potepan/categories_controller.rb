class Potepan::CategoriesController < ApplicationController
  def show
    @taxon = Spree::Taxon.find(params[:id])
    @taxonomies = Spree::Taxonomy.all.includes(:taxons)
    @products = @taxon.all_products.includes(master: %i[default_price images])
  end
end
