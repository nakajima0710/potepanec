module Potepan::ProductDecorator
  def related_products
    Spree::Product.in_taxons(taxons).includes(master: %i[default_price images]).where.not(id: id).distinct
  end
  Spree::Product.prepend self
end
