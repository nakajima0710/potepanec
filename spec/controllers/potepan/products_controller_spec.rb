require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe 'GET #show' do
    let(:taxon) { create(:taxon) }
    let!(:product) { create(:product, taxons: [taxon]) }

    before { get :show, params: { id: product.id } }

    it 'responds successfully' do
      expect(response).to have_http_status '200'
    end

    it 'shows @product' do
      expect(assigns(:product)).to eq product
    end

    it 'renders the :show' do
      expect(response).to render_template :show
    end
  end
end
