require 'rails_helper'

RSpec.describe Potepan::SampleController, type: :controller do
  describe '#index' do
    before do
      get :index
    end

    it 'responds successfully' do
      expect(response).to have_http_status '200'
    end

    it 'renders the :index' do
      expect(response).to render_template :index
    end
  end
end
