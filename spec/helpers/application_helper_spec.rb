require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe 'application title helper' do
    it 'displayed full title' do
      expect(full_title('title')).to eq 'title - BIGBAG store'
    end

    it 'title_method without page_title' do
      expect(full_title('')).to eq 'BIGBAG store'
      expect(full_title(nil)).to eq 'BIGBAG store'
    end
  end
end
