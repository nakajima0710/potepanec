require 'rails_helper'

RSpec.describe 'Categories_feature', type: :feature do
  let(:taxonomy)      { create(:taxonomy, name: 'Category') }
  let(:taxon)         { create(:taxon,    name: 'Taxon',       taxonomy: taxonomy, parent: taxonomy.root) }
  let(:other_taxon)   { create(:taxon,    name: 'Other_Taxon', taxonomy: taxonomy, parent: taxonomy.root) }
  let(:product)       { create(:product,  name: 'Product',       price: '11.11', taxons: [taxon]) }
  let(:other_product) { create(:product,  name: 'Other_Product', price: '99.99', taxons: [other_taxon]) }

  it 'view categories page' do
    visit potepan_category_path(taxonomy.root.id)
    expect(page).to have_title "#{taxonomy.name} - #{full_title}"
  end

  it '指定外のカテゴリーの商品が表示されていない' do
    visit potepan_category_path(taxon.id)
    expect(page).not_to have_content other_product.name
  end

  it '商品ページへのリンクが正常' do
    visit potepan_category_path(product.taxons.first.id)
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
    click_link product.name
    expect(page).to have_current_path potepan_product_path(product.id)
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
    expect(page).to have_content product.description
    expect(page).to have_link '一覧ページへ戻る', href: potepan_category_path(taxon.id)
  end
end
