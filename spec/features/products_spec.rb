require 'rails_helper'
include ApplicationHelper

RSpec.describe 'Products_feature', type: :feature do
  let(:product) do
    create(:product, name: 'Rails', price: '22.5')
  end

  let!(:taxon)            { create(:taxon) }
  let!(:product)          { create(:product, taxons: [taxon]) }
  let!(:product_fake)     { create(:product) }
  let!(:related_product)  { create(:product, taxons: [taxon]) }
  let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

  before { visit potepan_product_path(product.id) }

  it 'View show page' do
    visit potepan_product_path(product.id)
    expect(page).to have_title "#{product.name} - #{full_title}"
    within '.lightSection' do
      expect(page).to have_content(product.name, count: 2)
    end

    expect(page).to have_link 'HOME', href: potepan_index_path

    #関連商品が正常に表示されている
    within '.related-products' do
      expect(page).not_to have_content product.name
      related_products.first(4) do |related_product|
        expect(page).to have_content related_product.name
        expect(page).to have_content related_product.display_price
        # 商品リンクが機能する
        expect(page).to have_current_path potepan_product_path(related_product.id)
      end
      expect(page).to have_selector '.related-product', count: 4
    end
  end

  it '関連商品に不適切な商品が表示されていない' do
    within '.related-products' do
      # 関連商品は４つのみ表示されている
      expect(page).to have_selector '.related-product', count: 4
      # 関連商品にメインの商品は表示されていない
      expect(page).not_to have_content product.name
      expect(page).not_to have_content product_fake
    end
  end
end
